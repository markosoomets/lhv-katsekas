import {CommonFunctions} from "../common/commonFunctions";

export class MaksimaalneKuumaksePage {
    public open(): MaksimaalneKuumaksePage {
        cy.visit(`${CommonFunctions.getEnvUrl()}/et/liising#monthly-payment`);
        CommonFunctions.removeCookiesModal();
        //Tahaks kasutada cypressi reloadi peale cookie modaali eemaldamist aga tekib probleem selected aasta väärtusega sellisel juhul
        //cy.reload()
        cy.wait(500);
        return this;
    }

    public getPerekonnaseisCheckbox() {
        return cy.get('#marital-status-married');
    }

    public getYlalpeetavateArv() {
        return cy.get('#dependent-persons');
    }

    public getNetosissetulek() {
        return cy.get('#monthly-income');
    }

    public getKaastaotlejaPerekonnaseisCheckbox() {
        return cy.get('#guarantor-marital-status-married');
    }

    public getKaastaotlejaYlalpeetavateArv() {
        return cy.get('#guarantor-dependent-persons');
    }

    public getKaastaotlejanetosissetulek() {
        return cy.get('#guarantor-monthly-income');
    }
}