import {NaidisKuumaksePage} from "../../src/pageobject/naidisKuumaksePage";
import {CommonFunctions} from "../../src/common/commonFunctions";

context('Kalkulaatori navigatsiooni testid', () => {
    it(
        'Taotle liisingut nupp viib liisingu taotluse lehele', () => {
            let kalkulaator: NaidisKuumaksePage = new NaidisKuumaksePage().open();
            kalkulaator.getTaotleLiisingutButton().click();
            cy.url().should('include', 'et/liising/taotlus');
        }
    )

    it(
        'Maksegraafiku link suunab maksegraafiku lehele', () => {
            let kalkulaator: NaidisKuumaksePage = new NaidisKuumaksePage().open();
            //Navigeerimiseks kasutan visit funktsiooni sest lingi peale klikkimine avab uue tab'i
            kalkulaator.getMaksegraafikLink().invoke('attr', 'href').then(graafikLink => {
                cy.visit(`${CommonFunctions.getEnvUrl()}${graafikLink}`)
            })
            cy.url().should('include', 'et/liising/graafik')
        }
    )


})