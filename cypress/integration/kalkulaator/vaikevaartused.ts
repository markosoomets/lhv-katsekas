import {NaidisKuumaksePage} from "../../src/pageobject/naidisKuumaksePage";
import {NaidisKuumakseModel} from "../../src/model/naidisKuumakseModel";
import {AccountType} from "../../src/enum/accountType";
import {LeaseType} from "../../src/enum/leaseType";
import {PerioodYears} from "../../src/enum/perioodYears";
import {PerioodMonths} from "../../src/enum/perioodMonths";

context('Kalkulaatori vaikeväärtuste testid', () => {
    it('Kalkulaatoril on täidetud vaikeväärtused', () => {
        let kalkulaator: NaidisKuumaksePage = new NaidisKuumaksePage().open();
        let vaikevaartused: NaidisKuumakseModel = new NaidisKuumakseModel().default();
        kalkulaator.getSoovinLiisingutRadio(vaikevaartused._liisingIsik).should('be.checked');
        kalkulaator.getLiisingTyypRadio(vaikevaartused._liisingTyyp).should('be.checked');
        kalkulaator.getSoidukiHindField().should('have.value', vaikevaartused._soidukiHind);
        vaikevaartused._kaibemaksuga ? kalkulaator.getKaibemaksCheckbox().should('be.checked') : kalkulaator.getKaibemaksCheckbox().should('not.be.checked');
        kalkulaator.getSissemakseProtsentField().should('have.value', vaikevaartused._sissemakseProtsent);
        kalkulaator.getSissemakseEurField().should('have.value', vaikevaartused._sissemakseEur);
        kalkulaator.getPerioodAastatOption().should('have.value', vaikevaartused._liisinguPerioodAastat);
        kalkulaator.getPerioodKuudOption().should('have.value', vaikevaartused._liisinguPerioodKuud);
        kalkulaator.getIntressProtsentField().should('have.value', vaikevaartused._intressProtsent);
        kalkulaator.getJaakvaartusProtsentField().should('have.value', vaikevaartused._jaakVaartusProtsent);
        kalkulaator.getJaakvaartusEurField().should('have.value', vaikevaartused._jaakVaartusEur);
        kalkulaator.getKuumakseSumma().should('contain', vaikevaartused._kuumakseSummaVaartus);
    })

    it('Käibemaksu tasumine väli ilmub kui liisingu tüüp muuta juriidiliseks isikuks', () => {
        let kalkulaator: NaidisKuumaksePage = new NaidisKuumaksePage().open()
        let kalkulaatorModel: NaidisKuumakseModel = new NaidisKuumakseModel().default()
            .setLiisingIsik(AccountType.JURIIDILINE_ISIK)

        kalkulaator.setSoovinLiisingutRadio(kalkulaatorModel._liisingIsik)

        kalkulaator.getKaibemaksutasumineDropdown().should('be.visible', kalkulaatorModel._kaibemaksuTasumineTyyp);
        kalkulaator.getKaibemaksutasumineDropdown().should('have.value', kalkulaatorModel._kaibemaksuTasumineTyyp);
    })

    it('Käibemaksu tasumine väli kaob kui liisingu tüüp muuta juriidiliseks isikuks ja tagasi eraisikuks', () => {
        let kalkulaator: NaidisKuumaksePage = new NaidisKuumaksePage().open()
        let kalkulaatorModel: NaidisKuumakseModel = new NaidisKuumakseModel().default()
            .setLiisingIsik(AccountType.JURIIDILINE_ISIK);

        kalkulaator.setSoovinLiisingutRadio(kalkulaatorModel._liisingIsik);
        kalkulaatorModel.setLiisingIsik(AccountType.ERAISIK);

        kalkulaator.setSoovinLiisingutRadio(kalkulaatorModel._liisingIsik);
        kalkulaator.getKaibemaksutasumineDropdown().should('not.be.visible');
    })


    it('Liisingu tüübi muutmisel kasutusrendiks kaob perioodist kuude valik', () => {
        let kalkulaator: NaidisKuumaksePage = new NaidisKuumaksePage().open();
        let kalkulaatorModel: NaidisKuumakseModel = new NaidisKuumakseModel().default()
            .setLiisingTyyp(LeaseType.KASUTUSRENT);

        kalkulaator.setLiisingTyypRadio(kalkulaatorModel._liisingTyyp);

        kalkulaator.getPerioodKuudOption().should('not.be.visible');
    })

    it(`Kalkulaatori aastate valikute väärtused on 0-84`, () => {
        let kalkulaator: NaidisKuumaksePage = new NaidisKuumaksePage().open()
        kalkulaator.getPerioodAastatOption().within(() => {
                cy.get('option').each((item, index) => {
                    //Kontrollime perioodYears enumi vastu kõik väärtused ükshaaval üle
                    expect(Object.keys(PerioodYears)[index]).to.equal(item.val().toString())
                })
            }
        )
    })

//Eraldi test väärtuste arvu kontrolliks sest eelnev test ei leia olukordi kus nt 7 aasta väärtus on kadunud
    it(`Kalkulaatori aastate valikus on ${Object.keys(PerioodYears).length / 2} väärtust`, () => {
        let kalkulaator: NaidisKuumaksePage = new NaidisKuumaksePage().open()
        kalkulaator.getPerioodAastatOption().within(() => {
                //Object.keys tagastab enumi + väärtuse indexi koos sp. jagame tulemuste arvu 2'ga
                cy.get('option').should('have.length', Object.keys(PerioodYears).length / 2)
            }
        )
    })

    it(`Kalkulaatori kuude valikute väärtused on 0-12`, () => {
        let kalkulaator: NaidisKuumaksePage = new NaidisKuumaksePage().open()
        kalkulaator.getPerioodKuudOption().within(() => {
                cy.get('option').each((item, index) => {
                    //Kontrollime perioodMonths enumi vastu kõik väärtused ükshaaval üle
                    expect(Object.keys(PerioodMonths)[index]).to.equal(item.val().toString())
                })
            }
        )
    })

//Eraldi test väärtuste arvu kontrolliks sest eelnev test ei leia olukordi kus nt viimane väärtus on kadunud
    it(`Kalkulaatori kuude valikus on ${Object.keys(PerioodMonths).length / 2} väärtust`, () => {
        let kalkulaator: NaidisKuumaksePage = new NaidisKuumaksePage().open()
        kalkulaator.getPerioodKuudOption().within(() => {
                //Object.keys tagastab enumi + väärtuse indexi koos sp. jagame tulemuste arvu 2'ga
                cy.get('option').should('have.length', Object.keys(PerioodMonths).length / 2)
            }
        )
    })

    it('Disclaimer tekst on kuvatud', () => {
            new NaidisKuumaksePage().open()
            cy.get('#monthly-payment').within(() =>
                cy.get('.disclaimer > p')
                    .should('contain', 'Tulemus on ligikaudne ja võib erineda sulle pakutavatest tingimustest'))
        }
    )


})