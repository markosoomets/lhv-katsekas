import {NaidisKuumaksePage} from "../../src/pageobject/naidisKuumaksePage";
import {NaidisKuumakseModel} from "../../src/model/naidisKuumakseModel";

context('Kalkulaatori arvutusloogika ja väljade muutmine', () => {

    it('Sõiduki hinna muutmine - Sissemakse protsent 15% -> Sõiduki hind EUR 94483.43 -> Väärtustab sissemakse EUR 14172.51', () => {
            let kalkulaator: NaidisKuumaksePage = new NaidisKuumaksePage().open()
            let kalkulaatorModel = new NaidisKuumakseModel().default()
                .setSoidukiHind('94483.43')
                .setSissemakseProtsent('15')
                .setSissemakseEur('14172.51')

            kalkulaator.clearAndSetSissemakseProtsentField(kalkulaatorModel._sissemakseProtsent)
            kalkulaator.clearAndsetSoidukiHindField(kalkulaatorModel._soidukiHind)

            kalkulaator.getSoidukiHindField().should('have.value', kalkulaatorModel._soidukiHind)
            kalkulaator.getSissemakseProtsentField().should('have.value', kalkulaatorModel._sissemakseProtsent)
            kalkulaator.getSissemakseEurField().should('have.value', kalkulaatorModel._sissemakseEur)
            kalkulaator.getKuumakseSumma().should('contain', '1140.15');
        }
    )

    it('Sõiduki hinna muutmine - Jääkväärtuse protsent 20% -> Sõiduki hind EUR 4489221 - Väärtustab jääkväärtus EUR 897844.2', () => {

            let kalkulaator: NaidisKuumaksePage = new NaidisKuumaksePage().open()
            let kalkulaatorModel = new NaidisKuumakseModel().default()
                .setSoidukiHind('4489221')
                .setJaakVaartusProtsent('20')
                .setJaakVaartusEur('897844.2')

            kalkulaator.clearAndSetJaakvaartusProtsentField(kalkulaatorModel._jaakVaartusProtsent)
            kalkulaator.clearAndsetSoidukiHindField(kalkulaatorModel._soidukiHind)

            kalkulaator.getSoidukiHindField().should('have.value', kalkulaatorModel._soidukiHind)
            kalkulaator.getJaakvaartusProtsentField().should('have.value', kalkulaatorModel._jaakVaartusProtsent)
            kalkulaator.getJaakvaartusEurField().should('have.value', kalkulaatorModel._jaakVaartusEur)
            kalkulaator.getKuumakseSumma().should('contain', '52157.09');


        }
    )


    it('Jääkväärtuse protsent muutub 1 kui jääkväärtus EUR muutub 150', () => {

            let kalkulaator: NaidisKuumaksePage = new NaidisKuumaksePage().open()
            let kalkulaatorModel = new NaidisKuumakseModel().default()
                .setJaakVaartusEur('150')
                .setJaakVaartusProtsent('1')

            kalkulaator.clearAndSetJaakvaartusEurField(kalkulaatorModel._jaakVaartusEur)

            kalkulaator.getJaakvaartusProtsentField().should('have.value', kalkulaatorModel._jaakVaartusProtsent)
            kalkulaator.getJaakvaartusEurField().should('have.value', kalkulaatorModel._jaakVaartusEur)
            kalkulaator.getKuumakseSumma().should('contain', '209.36');

        }
    )

    it('Sissemakse protsent muutub 63.33 kui sissemakse EUR muutub 9500', () => {

            let kalkulaator: NaidisKuumaksePage = new NaidisKuumaksePage().open()
            let kalkulaatorModel = new NaidisKuumakseModel().default()
                .setSissemakseEur('9500')
                .setSissemakseProtsent('63.33')

            kalkulaator.clearAndSetSissemakseEurField(kalkulaatorModel._sissemakseEur)

            kalkulaator.getSissemakseProtsentField().should('have.value', kalkulaatorModel._sissemakseProtsent)
            kalkulaator.getSissemakseEurField().should('have.value', kalkulaatorModel._sissemakseEur)
            kalkulaator.getKuumakseSumma().should('contain', '67.58');

        }
    )
})