export enum PerioodMonths {
    NULL,
    YKS,
    KAKS,
    KOLM,
    NELI,
    VIIS,
    KUUS,
    SEITSE,
    KAHEKSA,
    YHEKSA,
    KYMME,
    YKSTEIST,
    KAKSTEIST
}