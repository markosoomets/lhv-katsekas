export class CommonFunctions {
    //Cookiede eemaldamiseks eraldi üldkastatav funktioon sest varem või hiljem on seda veel vaja tõenäoliselt
    public static removeCookiesModal(): void {
        cy.get('#pirukas').should('be.visible')
            .get('#acceptPirukas').click();
    }

    public static getEnvUrl(): string {
        return 'https://www.lhv.ee'
    }
}