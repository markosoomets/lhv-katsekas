import {NaidisKuumaksePage} from "../../src/pageobject/naidisKuumaksePage";
import {NaidisKuumakseModel} from "../../src/model/naidisKuumakseModel";
import {KalkulaatorMaksegraafikHelper} from "../../src/pageobject/helper/kalkulaatorMaksegraafikHelper";
import {PerioodYears} from "../../src/enum/perioodYears";
import {PerioodMonths} from "../../src/enum/perioodMonths";

context('Kalkulaatori maksegraafiku URLi muutumine', () => {

    it('Maksegraafiku URLi vaikeväärtused on väärtustatud', () => {
        let kalkulaator: NaidisKuumaksePage = new NaidisKuumaksePage().open();
        let vaikevaartused: NaidisKuumakseModel = new NaidisKuumakseModel().default();
        kalkulaator.getMaksegraafikLink().should('have.attr', 'href').and('include', `accountType=${vaikevaartused._liisingIsik}`)
        kalkulaator.getMaksegraafikLink().should('have.attr', 'href').and('include', `productType=${vaikevaartused._liisingTyyp}`)
        kalkulaator.getMaksegraafikLink().should('have.attr', 'href').and('include', `sum=${KalkulaatorMaksegraafikHelper.getMaksegraafikSum(vaikevaartused)}`)
        kalkulaator.getMaksegraafikLink().should('have.attr', 'href').and('include', `vat=${KalkulaatorMaksegraafikHelper.getVatValue(vaikevaartused)}`)
        kalkulaator.getMaksegraafikLink().should('have.attr', 'href').and('include', `rate=${vaikevaartused._intressProtsent}`)
        kalkulaator.getMaksegraafikLink().should('have.attr', 'href').and('include', `initial=${vaikevaartused._sissemakseProtsent}`)
        kalkulaator.getMaksegraafikLink().should('have.attr', 'href').and('include', `reminder=${vaikevaartused._jaakVaartusProtsent}`)
        kalkulaator.getMaksegraafikLink().should('have.attr', 'href').and('include', `payments=${KalkulaatorMaksegraafikHelper.getLiisingPerioodKuud(vaikevaartused)}`)
    })

    it('Maksegraafiku URL payments väärtus muutub kui liisingu periood muutub', () => {
        let kalkulaator: NaidisKuumaksePage = new NaidisKuumaksePage().open();
        let andmemudel: NaidisKuumakseModel = new NaidisKuumakseModel().default()
            .setLiisingPerioodAastat(PerioodYears.NELI)

        kalkulaator.setPerioodAastatOption(andmemudel._liisinguPerioodAastat)

        kalkulaator.getMaksegraafikLink().should('have.attr', 'href').and('include', `payments=${KalkulaatorMaksegraafikHelper.getLiisingPerioodKuud(andmemudel)}`)
    })


    it('Maksegraafiku URL payments väärtus muutub kui liisingu kuude arv muutub', () => {
        let kalkulaator: NaidisKuumaksePage = new NaidisKuumaksePage().open();
        let andmemudel: NaidisKuumakseModel = new NaidisKuumakseModel().default()
            .setLiisingPerioodKuud(PerioodMonths.KOLM);

        kalkulaator.setPerioodKuudOption(andmemudel._liisinguPerioodKuud)

        kalkulaator.getMaksegraafikLink().should('have.attr', 'href').and('include', `payments=${KalkulaatorMaksegraafikHelper.getLiisingPerioodKuud(andmemudel)}`)
    })


    it('Maksegraafiku URL payments väärtus muutub kui liisingu kuude arv muutub', () => {
        let kalkulaator: NaidisKuumaksePage = new NaidisKuumaksePage().open();
        let andmemudel: NaidisKuumakseModel = new NaidisKuumakseModel().default()
            .setLiisingPerioodAastat(PerioodYears.NELI)
            .setLiisingPerioodKuud(PerioodMonths.KOLM);

        kalkulaator.setPerioodAastatOption(andmemudel._liisinguPerioodAastat)
        kalkulaator.setPerioodKuudOption(andmemudel._liisinguPerioodKuud)

        kalkulaator.getMaksegraafikLink().should('have.attr', 'href').and('include', `payments=${KalkulaatorMaksegraafikHelper.getLiisingPerioodKuud(andmemudel)}`)
    })

    it('Maksegraafiku URL sum parameeter muutub kui sõiduku maksumus muutub', () => {
        let kalkulaator: NaidisKuumaksePage = new NaidisKuumaksePage().open();
        let andmemudel: NaidisKuumakseModel = new NaidisKuumakseModel().default()
            .setSoidukiHind('7500')

        kalkulaator.clearAndsetSoidukiHindField(andmemudel._soidukiHind);
        kalkulaator.getMaksegraafikLink().should('have.attr', 'href').and('include', `sum=${KalkulaatorMaksegraafikHelper.getMaksegraafikSum(andmemudel)}`)

    })

    it('Maksegraafiku URL vat ja sum parameetrid muutuvad kui käibemaksu checkboxi väärtus muutub', () => {
        let kalkulaator: NaidisKuumaksePage = new NaidisKuumaksePage().open();
        let andmemudel: NaidisKuumakseModel = new NaidisKuumakseModel().default()
            .setKaibemaksuga(false)

        kalkulaator.setKaibemaksCheckbox(andmemudel._kaibemaksuga);
        kalkulaator.getMaksegraafikLink().should('have.attr', 'href').and('include', `vat=${KalkulaatorMaksegraafikHelper.getVatValue(andmemudel)}`)
    })

})
