import {NaidisKuumakseModel} from "../../model/naidisKuumakseModel";

export class KalkulaatorMaksegraafikHelper {

    public static getMaksegraafikSum(inputModel: NaidisKuumakseModel) {
        let soidukiHindEur = Number(inputModel._soidukiHind.replace(/\s/g, ""))
        return inputModel._kaibemaksuga ? soidukiHindEur / 1.2 : soidukiHindEur
    }

    public static getVatValue(inputModel: NaidisKuumakseModel) {
        return inputModel._kaibemaksuga ? 20 : 0
    }

    public static getLiisingPerioodKuud(inputModel: NaidisKuumakseModel) {
        return inputModel._liisinguPerioodAastat + inputModel._liisinguPerioodKuud
    }

}