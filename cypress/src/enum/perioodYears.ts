export enum PerioodYears {
    NULL = 0,
    YKS = 12,
    KAKS = 24,
    KOLM = 36,
    NELI = 48,
    VIIS = 60,
    KUUS = 72,
    SEITSE = 84
}