### LHV katseülesanne

Kasutasin liisingu kalkulaatori testimiseks Cypressi raamistikku https://www.cypress.io/ koos typescriptiga

Testid asuvad cypress/integration/kalkulaator all

Käivitamiseks

```npm install```

```cypress open```

Cypressi rakendusest saab siis juba omakorda valida millist testfaili jooksutada ja runner näitab ära ka teostatud tegevused.
Vajadusel on võimalik ka käsurealt jooksutamine, aga hetkel sellesse ei süvenenud.

Üldised ideed - 
* Kasutasin page object + model lähenemist ->  Tähendab siis et kuval olevad elemendid on kättesaadavad 
page objecti kaudu, modelis on defineeritud elementide võimalikud väärtused - testi enda loogika asub puhtalt testi failis.

* Üldiste funktsioonide jaoks kasutusel ka commonfunctions klass näiteks antud juhul cookie modaali eemaldamiseks 
ja keskkonna urli väärtustamiseks

* Erinevad staatilised valikute väärtused on enumites src/enum