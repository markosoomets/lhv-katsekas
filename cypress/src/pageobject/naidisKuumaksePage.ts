import {CommonFunctions} from "../common/commonFunctions";
import {AccountType} from "../enum/accountType";
import {LeaseType} from "../enum/leaseType";
import {PerioodYears} from "../enum/perioodYears";
import {PerioodMonths} from "../enum/perioodMonths";

export class NaidisKuumaksePage {

    public open(): NaidisKuumaksePage {
        cy.visit(`${CommonFunctions.getEnvUrl()}/et/liising#monthly-payment`);
        CommonFunctions.removeCookiesModal();
        //Tahaks kasutada cypressi reloadi peale cookie modaali eemaldamist aga tekib probleem selected aasta väärtusega sellisel juhul
        //cy.reload()
        cy.wait(500);
        return this;
    }


    //Radio elemendi sisendiks soovime mudelis määratud välja tüüpi korrektse elemendi leidmiseks - Jah see võib tekitada probleeme kui kuvale tekib 2x sama väärtusega radio valikut
    public getSoovinLiisingutRadio(isikTyyp: AccountType): Cypress.Chainable<JQuery<HTMLElement>> {
        return cy.get(`input[value=${isikTyyp}]`);
    }

    public setSoovinLiisingutRadio(isikTyyp: AccountType): void {
        this.getSoovinLiisingutRadio(isikTyyp).check({force: true})
    }

    public getTaotleLiisingutButton(): Cypress.Chainable<JQuery<HTMLElement>> {
        return cy.get('#monthly-payment > .calculator > .calculator-result > .btn')
    }

    public getLiisingTyypRadio(liisingTyyp: LeaseType): Cypress.Chainable<JQuery<HTMLElement>> {
        return cy.get(`input[value=${liisingTyyp}]`);
    }

    public setLiisingTyypRadio(liisingTyyp: LeaseType): void {
        this.getLiisingTyypRadio(liisingTyyp).check({force: true})
    }

    public getMaksegraafikLink(): Cypress.Chainable<JQuery<HTMLElement>> {
        return cy.get('.calculator-result > .payment-graph-link');
    }

    public getKuumakseSumma(): Cypress.Chainable<JQuery<HTMLElement>> {
        return cy.get('#monthly-payment > .calculator > .calculator-result > .payment');
    }

    public getSoidukiHindField(): Cypress.Chainable<JQuery<HTMLElement>> {
        return cy.get('#price');
    }

    public clearAndsetSoidukiHindField(input: string): void {
        this.getSoidukiHindField().clear()
        this.getSoidukiHindField().type(input);
    }

    public getKaibemaksutasumineDropdown(): Cypress.Chainable<JQuery<HTMLElement>> {
        return cy.get('#vat_scheduling');
    }

    public getSissemakseProtsentField(): Cypress.Chainable<JQuery<HTMLElement>> {
        return cy.get('#initial_percentage');
    }

    public clearAndSetSissemakseProtsentField(input: string): void {
        this.getSissemakseProtsentField().clear()
        this.getSissemakseProtsentField().type(input);
    }

    public getKaibemaksCheckbox(): Cypress.Chainable<JQuery<HTMLElement>> {
        return cy.get('#vat_included');
    }

    public setKaibemaksCheckbox(input: boolean): void {
        input ? this.getKaibemaksCheckbox().check({force: true}) : this.getKaibemaksCheckbox().uncheck({force: true})
    }

    public getSissemakseEurField(): Cypress.Chainable<JQuery<HTMLElement>> {
        return cy.get('#initial');
    }

    public clearAndSetSissemakseEurField(input: string): void {
        this.getSissemakseEurField().clear();
        this.getSissemakseEurField().type(input);
    }

    public getPerioodAastatOption(): Cypress.Chainable<JQuery<HTMLElement>> {
        return cy.get('select[name="years"]');
    }

    public setPerioodAastatOption(input: PerioodYears): void {
        this.getPerioodAastatOption().select(input.toString());
    }

    public getPerioodKuudOption(): Cypress.Chainable<JQuery<HTMLElement>> {
        return cy.get('select[name="months"]');
    }

    public setPerioodKuudOption(input: PerioodMonths): void {
        this.getPerioodKuudOption().select(input.toString());
    }


    public getIntressProtsentField(): Cypress.Chainable<JQuery<HTMLElement>> {
        return cy.get('#interest_rate');
    }

    public clearAndSetIntressProtsentField(input: string): void {
        this.getIntressProtsentField().clear()
        this.getIntressProtsentField().type(input);
    }

    public getJaakvaartusProtsentField(): Cypress.Chainable<JQuery<HTMLElement>> {
        return cy.get('#reminder_percentage');
    }


    public clearAndSetJaakvaartusProtsentField(input: string): void {
        this.getJaakvaartusProtsentField().clear();
        this.getJaakvaartusProtsentField().type(input);
    }

    public getJaakvaartusEurField(): Cypress.Chainable<JQuery<HTMLElement>> {
        return cy.get('#reminder');
    }

    public clearAndSetJaakvaartusEurField(input: string): void {
        this.getJaakvaartusEurField().clear()
        this.getJaakvaartusEurField().type(input);
    }
}