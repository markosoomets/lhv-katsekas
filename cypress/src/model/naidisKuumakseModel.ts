import {LeaseType} from "../enum/leaseType";
import {AccountType} from "../enum/accountType";
import {PerioodYears} from "../enum/perioodYears";
import {PerioodMonths} from "../enum/perioodMonths";
import {VatSchedulingType} from "../enum/vatSchedulingType";

export class NaidisKuumakseModel {
    _liisingIsik: AccountType = AccountType.ERAISIK
    _liisingTyyp: LeaseType = LeaseType.KAPITALIRENT
    _soidukiHind: string = "15 000"
    _kaibemaksuTasumineTyyp: VatSchedulingType = VatSchedulingType.KOOS_SISSEMAKSEGA
    _kaibemaksuga: boolean = true
    _sissemakseProtsent: string = "10"
    _sissemakseEur: string = "1500"
    _liisinguPerioodAastat: PerioodYears = PerioodYears.KUUS
    _liisinguPerioodKuud: PerioodMonths = PerioodMonths.NULL
    _intressProtsent: string = "4"
    _jaakVaartusProtsent: string = "10"
    _jaakVaartusEur: string = "1500"
    _kuumakseSummaVaartus:string = "192.74"


    default(): NaidisKuumakseModel {
        return new NaidisKuumakseModel;
    }


    setLiisingIsik(value: AccountType): NaidisKuumakseModel {
        this._liisingIsik = value;
        return this;
    }

    setLiisingTyyp(value: LeaseType): NaidisKuumakseModel {
        this._liisingTyyp = value;
        return this;
    }

    setSoidukiHind(value: string): NaidisKuumakseModel {
        this._soidukiHind = value;
        return this;
    }

    setKaibemaksuga(value: boolean): NaidisKuumakseModel {
        this._kaibemaksuga = value;
        return this;
    }

    setSissemakseProtsent(value: string): NaidisKuumakseModel {
        this._sissemakseProtsent = value;
        return this;
    }

    setSissemakseEur(value: string): NaidisKuumakseModel {
        this._sissemakseEur = value;
        return this;
    }

    setLiisingPerioodAastat(value: PerioodYears): NaidisKuumakseModel {
        this._liisinguPerioodAastat = value;
        return this
    }

    setLiisingPerioodKuud(value: PerioodMonths): NaidisKuumakseModel {
        this._liisinguPerioodKuud = value;
        return this
    }


    setIntressProtsent(value: string): NaidisKuumakseModel {
        this._intressProtsent = value;
        return this;
    }

    setJaakVaartusProtsent(value: string): NaidisKuumakseModel {
        this._jaakVaartusProtsent = value;
        return this;
    }

    setJaakVaartusEur(value: string): NaidisKuumakseModel {
        this._jaakVaartusEur = value;
        return this;
    }
}